package com.innovencture.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.innovencture.services.IWeatherService;

@Controller
public class ApplicationController {

	private static final Logger _log = LoggerFactory.getLogger(ApplicationController.class);
	
	@Autowired
	private IWeatherService weatherService;

	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView showLoginPage(ModelMap model) {
		_log.info("Welcome Page");
		return new ModelAndView("welcome");
	}
	
	@RequestMapping(value="/getColdestHour", method = RequestMethod.GET)
	@ResponseBody public String getColdestHour(@RequestParam("zipCode")String zipCode) {
		String coldestHour = null;
		_log.info("Method REST Controller ----> getColdestHour");
		coldestHour = this.weatherService.getColdestHourByZipCode(zipCode);
		return coldestHour;
	}
	
}