package com.innovencture.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.innovencture.dto.WeatherAPIResponse;
import com.innovencture.services.WeatherServiceImpl;

/**
 * 
 * @author Dipen
 *
 */
@Repository("weatherDao")
public class WeatherDAOImpl implements IWeatherDAO {

	private static final Logger _log = LoggerFactory.getLogger(WeatherServiceImpl.class);
	
	/**
	 * 
	 * @param zipCode
	 * @return
	 */
	public WeatherAPIResponse getColdestHourByZipCode(String zipCode) {
		_log.info("Inside getColderHour Dao");
		RestTemplate restTemplate = new RestTemplate();
		WeatherAPIResponse response = null;
		try {
			response = restTemplate.getForObject(
					"https://weather.cit.api.here.com/weather/1.0/report.json?product=forecast_hourly&zipcode="+zipCode+"&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg",
					WeatherAPIResponse.class);			
		}catch (Exception e) {
			_log.error("Error in getCodestHour DAO "+e);
			response = new WeatherAPIResponse();
			response.setType("Invalid Request");
		}
		System.out.println(response);
		return response;
	}

}
