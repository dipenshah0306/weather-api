package com.innovencture.dao;

import com.innovencture.dto.WeatherAPIResponse;

/**
 * 
 * @author Dipen
 *
 */
public interface IWeatherDAO {

	/**
	 * 
	 * @param zipCode
	 * @return
	 */
	public WeatherAPIResponse getColdestHourByZipCode(String zipCode); 
}
