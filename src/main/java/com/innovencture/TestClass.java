package com.innovencture;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TestClass {

	public static void main(String[] args) throws ParseException {
		/*
		 * Date currentDateTime = new Date(); currentDateTime.setHours(00);
		 * currentDateTime.setMinutes(00); currentDateTime.setSeconds(01);
		 * System.out.println("Current By Date and Time : "+currentDateTime); DateFormat
		 * format = new SimpleDateFormat("yyyy-MMM-dd");
		 * format.setTimeZone(TimeZone.getTimeZone("UTC")); String utcDateTimeString =
		 * format.format(currentDateTime); Date utDate =
		 * (Date)format.parse(utcDateTimeString);
		 * //System.out.println("Using Instant Time : "+Instant.now().toString());
		 * System.out.println("UTC Date is : "+utDate);
		 */

		/*
		 * Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		 * 
		 * calendar.set(Calendar.HOUR,0); calendar.set(Calendar.MINUTE, 0);
		 * calendar.set(Calendar.SECOND,0); calendar.set(Calendar.MILLISECOND, 1);
		 * 
		 * Date currentDate = calendar.getTime();
		 * System.out.println("Customized Todays Date : "+currentDate);
		 */
		/*
		 * String currentTimeString = format.format(currentDate); Date utCDate =
		 * (Date)format.parse(currentTimeString);
		 * System.out.println("Customized UTC Date : "+utCDate);
		 */
		
		/*
		 * TimeZone timeZone = TimeZone.getTimeZone("UTC"); Calendar calendar =
		 * Calendar.getInstance(timeZone); SimpleDateFormat simpleDateFormat = new
		 * SimpleDateFormat("EE MMM dd HH:mm:ss zzz yyyy", Locale.US);
		 * simpleDateFormat.setTimeZone(timeZone);
		 * calendar.set(Calendar.AM_PM,Calendar.AM); calendar.set(Calendar.HOUR,0);
		 * calendar.set(Calendar.MINUTE, 0); calendar.set(Calendar.SECOND,0);
		 * calendar.set(Calendar.MILLISECOND, 1);
		 * 
		 * System.out.println("Calendar Time : "+calendar.getTime());
		 * 
		 * String utcTime = simpleDateFormat.format(calendar.getTime());
		 * 
		 * Date utcDate = simpleDateFormat.parse(utcTime);
		 * System.out.println("Expected Date : "+utcTime);
		 * System.out.println("Got Date is : "+utcDate);
		 * 
		 * // System.out.println("Calendar UTC Time : "+calendar.getTime);
		 * System.out.println("UTC:     " +
		 * simpleDateFormat.format(calendar.getTime()));
		 */
		//System.out.println("Default: " + calendar.getTime());
		
		
		/*
		 * Calendar calendar = Calendar.getInstance(); calendar.set(Calendar.AM_PM,
		 * Calendar.AM); calendar.set(Calendar.HOUR, 0); calendar.set(Calendar.MINUTE,
		 * 0); calendar.set(Calendar.SECOND, 0); calendar.set(Calendar.MILLISECOND, 1);
		 */
		
	
	}
}
