package com.innovencture.services;

/**
 * 
 * @author Dipen
 *
 */
public interface IWeatherService {

	/**
	 * 
	 * @param zipCode
	 */
	public String getColdestHourByZipCode(String zipCode);
}
