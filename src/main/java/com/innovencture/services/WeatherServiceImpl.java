package com.innovencture.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innovencture.dao.IWeatherDAO;
import com.innovencture.dto.Forecast;
import com.innovencture.dto.WeatherAPIResponse;

/**
 * 
 * @author Dipen
 *
 */
@Service("weatherService")
public class WeatherServiceImpl implements IWeatherService {

	private static final Logger _log = LoggerFactory.getLogger(WeatherServiceImpl.class);

	@Autowired
	private IWeatherDAO weatherDao;

	/**
	 * 
	 * @param zipCode
	 */
	public String getColdestHourByZipCode(String zipCode) {
		String coldHour = null;
		_log.info("Inside GetColdestHour Services");
		WeatherAPIResponse weatherAPIResponse = this.weatherDao.getColdestHourByZipCode(zipCode);
		if (null != weatherAPIResponse) {
			if (null != weatherAPIResponse.getHourlyForecasts()
					&& !weatherAPIResponse.getHourlyForecasts().getForecastLocation().getForecast().isEmpty()) {
				List<Forecast> hourlyForecasts = weatherAPIResponse.getHourlyForecasts().getForecastLocation()
						.getForecast();
				coldHour = getColdestHourProcessed(hourlyForecasts);
			} else if (null != weatherAPIResponse.getType()) {
				coldHour = "Invalid input data";
			}
		} else {
			coldHour = "Service Unavailable, Please try after Sometime";
		}

		return coldHour;
	}

	public String getColdestHourProcessed(List<Forecast> forecastHourList) {
		boolean isStartedCount = false;
		int count = 0;
		String coldestHour = "";
		double coldestTemp = 0;
		for (Forecast forecastedHour : forecastHourList) {
			int hour = Integer.parseInt(forecastedHour.getLocalTime().substring(0, 2));
			if (hour > 0 && !isStartedCount) {
				continue;
			} else {
				if (count == 24) {
					break;
				}
				if (hour == 0) {
					isStartedCount = true;
				}
				double temperature = Double.parseDouble(forecastedHour.getTemperature());
				if (coldestTemp == 0 || coldestTemp > temperature) {
					coldestTemp = temperature;
					coldestHour = hour + " hours";
				}
				count++;
				_log.info("Hour is : " + forecastedHour.getLocalTime());
			}
		}
		return coldestHour;
	}
}
