package com.innovencture.dto;

import java.io.Serializable;

public class Forecast implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3198794741444898740L;

	private String daylight;
	private String description;
	private String temperature;
	private String dayOfWeek;
	private String weekday;
	private String utcTime;
	private String localTime;
	private String localTimeFormat;

	public String getDaylight() {
		return daylight;
	}

	public void setDaylight(String daylight) {
		this.daylight = daylight;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public String getWeekday() {
		return weekday;
	}

	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}

	public String getUtcTime() {
		return utcTime;
	}

	public void setUtcTime(String utcTime) {
		this.utcTime = utcTime;
	}

	public String getLocalTime() {
		return localTime;
	}

	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}

	public String getLocalTimeFormat() {
		return localTimeFormat;
	}

	public void setLocalTimeFormat(String localTimeFormat) {
		this.localTimeFormat = localTimeFormat;
	}

	@Override
	public String toString() {
		return "Forecast [daylight=" + daylight + ", description=" + description + ", temperature=" + temperature
				+ ", dayOfWeek=" + dayOfWeek + ", weekday=" + weekday + ", utcTime=" + utcTime + ", localTime="
				+ localTime + ", localTimeFormat=" + localTimeFormat + "]";
	}
}
