package com.innovencture.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Dipen
 *
 */
public class WeatherAPIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7005064219543242056L;

	private HourlyForecast hourlyForecasts;
	private Date feedCreation;
	private Boolean metric;
	private String Type;
	private String Message;

	public HourlyForecast getHourlyForecasts() {
		return hourlyForecasts;
	}

	public void setHourlyForecasts(HourlyForecast hourlyForecasts) {
		this.hourlyForecasts = hourlyForecasts;
	}

	public Date getFeedCreation() {
		return feedCreation;
	}

	public void setFeedCreation(Date feedCreation) {
		this.feedCreation = feedCreation;
	}

	public Boolean getMetric() {
		return metric;
	}

	public void setMetric(Boolean metric) {
		this.metric = metric;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	@Override
	public String toString() {
		return "WeatherAPIResponse [hourlyForecasts=" + hourlyForecasts + ", feedCreation=" + feedCreation + ", metric="
				+ metric + ", Type=" + Type + ", Message=" + Message + "]";
	}

}
