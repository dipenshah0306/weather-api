package com.innovencture.dto;

import java.io.Serializable;

/**
 * 
 * @author Dipen
 *
 */
public class HourlyForecast implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8787402491496927944L;

	private ForecastLocation forecastLocation;

	public ForecastLocation getForecastLocation() {
		return forecastLocation;
	}

	public void setForecastLocation(ForecastLocation forecastLocation) {
		this.forecastLocation = forecastLocation;
	}

	@Override
	public String toString() {
		return "HourlyForecast [forecastLocation=" + forecastLocation + "]";
	}
}
