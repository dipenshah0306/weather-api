package com.innovencture.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Dipen
 *
 */
public class ForecastLocation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4182225771665442238L;

	private List<Forecast> forecast;

	private String country;
	private String state;
	private String city;
	private Integer timezone;

	public List<Forecast> getForecast() {
		return forecast;
	}

	public void setForecast(List<Forecast> forecast) {
		this.forecast = forecast;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getTimezone() {
		return timezone;
	}

	public void setTimezone(Integer timezone) {
		this.timezone = timezone;
	}

	@Override
	public String toString() {
		return "ForecastLocation [forecast=" + forecast + ", country=" + country + ", state=" + state + ", city=" + city
				+ ", timezone=" + timezone + "]";
	}
}
